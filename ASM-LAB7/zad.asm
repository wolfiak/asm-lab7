.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
GetCurrentDirectoryA PROTO :DWORD, :DWORD
CreateDirectoryA PROTO :DWORD, :DWORD


.DATA

	write DWORD ?
	read DWORD ?
	
	dlugoscBufora DWORD 255
	bufor BYTE 255 DUP(0)
	lZnakow DWORD 0

	lokalizacja BYTE "c:\GrupaDupa",0

.CODE
main proc
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov read, EAX

	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov write, EAX

	invoke GetCurrentDirectoryA, dlugoscBufora, OFFSET bufor
	invoke WriteConsoleA, write, OFFSET bufor, dlugoscBufora, OFFSET lZnakow, 0

	invoke CreateDirectoryA, OFFSET lokalizacja, 0


	invoke ExitProcess, 0

main endp

END